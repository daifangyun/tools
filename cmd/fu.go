package cmd

import (
	"fmt"
	"strconv"

	"github.com/golang-module/carbon/v2"
	"github.com/spf13/cobra"
)

// fuCmd represents the fu command
var fuCmd = &cobra.Command{
	Use:   "fu",
	Short: "unix时间戳转为可读的时间格式",
	Long:  `unix时间戳转为可读的时间格式`,
	Run: func(cmd *cobra.Command, args []string) {
		unixTime, err := strconv.Atoi(args[0])
		if err != nil {
			fmt.Println("UnixTime时间戳异常!")
		}

		fmt.Println(carbon.CreateFromTimestamp(int64(unixTime)).ToDateTimeString())
	},
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) > 1 {
			return fmt.Errorf("accepts at most %d arg(s), received %d", 1, len(args))
		}
		return nil
	},
	Version: "1",
}

func init() {
	rootCmd.AddCommand(fuCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// fuCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// fuCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
