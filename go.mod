module gitee.com/daifangyun/tools

go 1.17

require (
	github.com/golang-module/carbon/v2 v2.2.1
	github.com/spf13/cobra v1.6.1
)

require (
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
